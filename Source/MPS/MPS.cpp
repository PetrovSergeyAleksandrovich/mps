// Copyright Epic Games, Inc. All Rights Reserved.

#include "MPS.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, MPS, "MPS" );

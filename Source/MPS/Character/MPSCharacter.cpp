// Fill out your copyright notice in the Description page of Project Settings.


#include "MPSCharacter.h"

#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/WidgetComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "MPS/Weapons/MPSWeapon.h"
#include "Net/UnrealNetwork.h"
#include "Kismet/KismetMathLibrary.h"

AMPSCharacter::AMPSCharacter()
{
	PrimaryActorTick.bCanEverTick = true;

	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArmComponent"));
	CameraBoom->SetupAttachment(GetMesh());
	CameraBoom->TargetArmLength = 600.0f;
	CameraBoom->bUsePawnControlRotation = true;

	FollowingCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowingCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	FollowingCamera->bUsePawnControlRotation = false;

	bUseControllerRotationYaw = true;
	GetCharacterMovement()->bOrientRotationToMovement = true;

	OverheadWidget = CreateDefaultSubobject<UWidgetComponent>("OverheadWidget");
	OverheadWidget->SetupAttachment(RootComponent);

	Combat = CreateDefaultSubobject<UMPSCombatComponent>("CombatComponent");
	Combat->SetIsReplicated(true);
	Combat->OwnerCharacter = this;

	GetCharacterMovement()->NavAgentProps.bCanCrouch = true;
	GetCapsuleComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECollisionResponse::ECR_Ignore);
	GetMesh()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECollisionResponse::ECR_Ignore);
}

void AMPSCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(AMPSCharacter, OverlappingWeapon, COND_OwnerOnly);

}



void AMPSCharacter::BeginPlay()
{
	Super::BeginPlay();
}


void AMPSCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	AimOffset(DeltaTime);
}

void AMPSCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);


	PlayerInputComponent->BindAxis("MoveForward", this, &AMPSCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AMPSCharacter::MoveRight);
	PlayerInputComponent->BindAxis("Turn", this, &AMPSCharacter::Turn);
	PlayerInputComponent->BindAxis("LookUp", this, &AMPSCharacter::LookUp);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AMPSCharacter::Jump);
	PlayerInputComponent->BindAction("Equip", IE_Pressed, this, &AMPSCharacter::EquipButtonPressed);
	PlayerInputComponent->BindAction("Crouch", IE_Pressed, this,  &AMPSCharacter::CrouchButtonPressed);
	PlayerInputComponent->BindAction("Aim", IE_Pressed, this, &AMPSCharacter::AimButtonPressed);
	PlayerInputComponent->BindAction("Aim", IE_Released, this, &AMPSCharacter::AimButtonReleased);
}

void AMPSCharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if (Combat)
	{
		Combat->OwnerCharacter = this; //sets owner for combat component
	}
}


void AMPSCharacter::MoveForward(float InValue)
{
	if (Controller && InValue != 0.0f)
	{
		const FRotator YawRotation(0.f, Controller->GetControlRotation().Yaw, 0.f);
		const FVector Direction( FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X) );
		AddMovementInput(Direction, InValue);
	}
}

void AMPSCharacter::MoveRight(float InValue)
{
	if (Controller && InValue != 0.0f)
	{
		const FRotator YawRotation(0.f, Controller->GetControlRotation().Yaw, 0.f);
		const FVector Direction( FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y) );
		AddMovementInput(Direction, InValue);
	}
}

void AMPSCharacter::Turn(float InValue)
{
	AddControllerYawInput(InValue);
}

void AMPSCharacter::LookUp(float InValue)
{
	AddControllerPitchInput(InValue);
}

void AMPSCharacter::EquipButtonPressed()
{
	if (Combat)
	{
		if (HasAuthority())
		{
			Combat->EquipWeapon(OverlappingWeapon);
		}
		else
		{
			ServerEquipButtonPressed();
		}
	}
}

void AMPSCharacter::CrouchButtonPressed()
{
	if(bIsCrouched)
	{
		IsInCrouchedState = false;
		UnCrouch();
		CrouchCall();
		return;
	}

	if(!IsInAir)
	{
		IsInCrouchedState = true;
		Crouch();
		CrouchCall();
	}
}

void AMPSCharacter::AimButtonPressed()
{
	if (Combat)
	{
		Combat->SetAiming(true);
	}
}

void AMPSCharacter::AimButtonReleased()
{
	if (Combat)
	{
		Combat->SetAiming(false);
	}
}

void AMPSCharacter::AimOffset(float DeltaTime)
{
	if(Combat && Combat->EquippedWeapon == nullptr) return;

	FVector Velocity = GetVelocity();
	Velocity.Z = 0.0f;
	float Speed = Velocity.Size();
	bool bIsInAir = GetCharacterMovement()->IsFalling();

	if (Speed == 0.0f && !bIsInAir) // standing still, not jumping
	{
		FRotator CurrentAimRotation = FRotator(0.0f, GetBaseAimRotation().Yaw, 0.0f);
		FRotator DeltaAimRotation = UKismetMathLibrary::NormalizedDeltaRotator(CurrentAimRotation, StartingAimRotation);
		AOYaw = DeltaAimRotation.Yaw;
		bUseControllerRotationYaw = false;
	}

	if (Speed > 0.0f || bIsInAir) //running or jumping
	{
		StartingAimRotation = FRotator(0.0f, GetBaseAimRotation().Yaw, 0.0f);
		AOYaw = 0.0f;
		bUseControllerRotationYaw = true;
	}

	AOPitch = GetBaseAimRotation().Pitch;
	if(AOPitch > 90.0f && !IsLocallyControlled())
	{
		//map pitch from  [270, 360) to [-90, 0)
		FVector2D InRange(270.f, 360.f);
		FVector2D OutRange(-90.0f, 0.f);

		AOPitch = FMath::GetMappedRangeValueClamped(InRange, OutRange, AOPitch);
	}
}

void AMPSCharacter::ServerEquipButtonPressed_Implementation()
{
	if (Combat)
	{
		Combat->EquipWeapon(OverlappingWeapon);
	}
}

void AMPSCharacter::OnRep_OverlappingWeapon(AMPSWeapon* LastWeapon)
{
	if (OverlappingWeapon)
	{
		OverlappingWeapon->ShowPickupWidget(true);
	}

	if (LastWeapon)
	{
		LastWeapon->ShowPickupWidget(false);
	}
}

void AMPSCharacter::SetOverlappingWeapon(AMPSWeapon* Weapon)
{
	if (OverlappingWeapon)
	{
		OverlappingWeapon->ShowPickupWidget(false);
	}

	OverlappingWeapon = Weapon;

	if (IsLocallyControlled())
	{
		if (OverlappingWeapon)
		{
			OverlappingWeapon->ShowPickupWidget(true);
		}
	}
}

bool AMPSCharacter::IsWeaponEquipped()
{
	if (Combat && Combat->EquippedWeapon)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool AMPSCharacter::IsAiming()
{
	return Combat && Combat->bAiming;
}

float AMPSCharacter::GetAOPitch() const
{
	return AOPitch;
}

AMPSWeapon* AMPSCharacter::GetEquippedWeapon() const
{
	if (Combat == nullptr)
	{
		return nullptr;
	}

	return Combat->EquippedWeapon;
}

float AMPSCharacter::GetAOYaw() const
{
	return AOYaw;
}


// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "MPS/PlayerComponents/MPSCombatComponent.h"
#include "MPSCharacter.generated.h"

class USpringArmComponent;
class UCameraComponent;
class AMPSWeapon;

UCLASS()
class MPS_API AMPSCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	AMPSCharacter();

	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	virtual void PostInitializeComponents() override;

protected:
	virtual void BeginPlay() override;

	void MoveForward(float InValue);
	void MoveRight(float InValue);
	void Turn(float InValue);
	void LookUp(float InValue);
	void EquipButtonPressed();
	void CrouchButtonPressed();
	void AimButtonPressed();
	void AimButtonReleased();
	void AimOffset(float DeltaTime);

public:

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Camera")
	USpringArmComponent* CameraBoom = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	UCameraComponent* FollowingCamera = nullptr;

private:

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UWidgetComponent* OverheadWidget;

	UPROPERTY(ReplicatedUsing = OnRep_OverlappingWeapon)
	AMPSWeapon* OverlappingWeapon;

	UFUNCTION()
	void OnRep_OverlappingWeapon(AMPSWeapon* LastWeapon);

	UPROPERTY(VisibleAnywhere)
	class UMPSCombatComponent* Combat;

	UFUNCTION(Server, Reliable)
	void ServerEquipButtonPressed();

	void ServerEquipButtonPressed_Implementation();

	float AOYaw;
	float AOPitch;
	FRotator StartingAimRotation;

public:

	void SetOverlappingWeapon(AMPSWeapon* Weapon);

	bool IsWeaponEquipped();

	bool IsAiming();

	FORCEINLINE float GetAOYaw() const;
	FORCEINLINE float GetAOPitch() const;
	AMPSWeapon* GetEquippedWeapon() const;

	UFUNCTION(BlueprintImplementableEvent)
	void CrouchCall();

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool IsInCrouchedState = false;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool IsInAir = false;
};



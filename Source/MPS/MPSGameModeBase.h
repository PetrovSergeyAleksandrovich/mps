// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MPSGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class MPS_API AMPSGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};

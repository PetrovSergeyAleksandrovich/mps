// Fill out your copyright notice in the Description page of Project Settings.


#include "MPSOverheadWidget.h"

void UMPSOverheadWidget::SetDisplayText(FString TextToDisplay)
{
	if (DisplayText)
	{
		//DisplayText->SetText(FText::FromString(TextToDisplay));
	}
}

void UMPSOverheadWidget::ShowPlayerNetRole(APawn* InPawn)
{
	ENetRole RemoteRole = InPawn->GetRemoteRole();
	FString Role;

	if (RemoteRole == ENetRole::ROLE_Authority)
	{
		Role = FString("Role Authority");
	}
	else if (RemoteRole == ENetRole::ROLE_SimulatedProxy)
	{
		Role = FString("Role SimulatedProxy");
	}
	else if (RemoteRole == ENetRole::ROLE_AutonomousProxy)
	{
		Role = FString("Role AutonomousProxy");
	}
	else
	{
		Role = FString("Role None");
	}

	FString RemoteRoleString = FString("Remote Role: " + Role);

	SetDisplayText(RemoteRoleString);
}

void UMPSOverheadWidget::NativeDestruct()
{
	RemoveFromParent();
	Super::NativeDestruct();
}

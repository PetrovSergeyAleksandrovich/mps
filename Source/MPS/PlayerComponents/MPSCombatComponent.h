// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "MPSCombatComponent.generated.h"

class AMPSWeapon;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MPS_API UMPSCombatComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UMPSCombatComponent();
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	friend class AMPSCharacter; // this allows access to this class for every section for AMPSCharacter class

	void EquipWeapon(AMPSWeapon* WeaponToEquip);

protected:
	virtual void BeginPlay() override;

	void SetAiming(bool bIsAiming);

	UFUNCTION(Server, Reliable)
	void ServerSetAiming(bool bIsAiming);

	UFUNCTION()
	void OnRep_EquippedWeapon();

private:

	AMPSCharacter* OwnerCharacter; //Character who owns this CombatComponent class

	UPROPERTY(ReplicatedUsing = OnRep_EquippedWeapon)
	AMPSWeapon* EquippedWeapon {nullptr};

	UPROPERTY(Replicated)
	bool bAiming {false};

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "MovementSettings", meta = (AllowPrivateAccess = "true"))
	float BaseWalkSpeed{600.f};

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "MovementSettings", meta = (AllowPrivateAccess = "true"))
	float AimWalkSpeed{400.f};
		
};

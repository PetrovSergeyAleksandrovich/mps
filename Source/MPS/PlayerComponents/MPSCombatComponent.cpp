// Fill out your copyright notice in the Description page of Project Settings.


#include "MPSCombatComponent.h"

#include "Engine/SkeletalMeshSocket.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "MPS/Character/MPSCharacter.h"
#include "MPS/Weapons/MPSWeapon.h"
#include "Net/UnrealNetwork.h"

UMPSCombatComponent::UMPSCombatComponent()
{
	PrimaryComponentTick.bCanEverTick = false;

}

void UMPSCombatComponent::BeginPlay()
{
	Super::BeginPlay();

	if(OwnerCharacter)
	{
		OwnerCharacter->GetCharacterMovement()->MaxWalkSpeed = BaseWalkSpeed;
	}
}

void UMPSCombatComponent::SetAiming(bool bIsAiming)
{
	bAiming = bIsAiming;
	ServerSetAiming(bIsAiming);

	if(OwnerCharacter)
	{
		OwnerCharacter->GetCharacterMovement()->MaxWalkSpeed = bIsAiming ? AimWalkSpeed : BaseWalkSpeed;
	}
}

void UMPSCombatComponent::ServerSetAiming_Implementation(bool bIsAiming)
{
	bAiming = bIsAiming;
	if (OwnerCharacter)
	{
		OwnerCharacter->GetCharacterMovement()->MaxWalkSpeed = bIsAiming ? AimWalkSpeed : BaseWalkSpeed;
	}
}

void UMPSCombatComponent::OnRep_EquippedWeapon()
{
	if(EquippedWeapon && OwnerCharacter)
	{
		OwnerCharacter->GetCharacterMovement()->bOrientRotationToMovement = false;
		OwnerCharacter->bUseControllerRotationYaw = true;
	}
}

void UMPSCombatComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

}

void UMPSCombatComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UMPSCombatComponent, EquippedWeapon);
	DOREPLIFETIME(UMPSCombatComponent, bAiming);
}

void UMPSCombatComponent::EquipWeapon(AMPSWeapon* WeaponToEquip)
{
	if (!OwnerCharacter || !WeaponToEquip)
	{
		return;
	}

	EquippedWeapon = WeaponToEquip;
	EquippedWeapon->SetWeaponState(EWeaponState::EWS_Equipped);

	const USkeletalMeshSocket* WeaponHand = OwnerCharacter->GetMesh()->GetSocketByName(FName("WeaponPoint"));
	if (WeaponHand)
	{
		WeaponHand->AttachActor(EquippedWeapon, OwnerCharacter->GetMesh());
	}

	EquippedWeapon->SetOwner(OwnerCharacter);
}

